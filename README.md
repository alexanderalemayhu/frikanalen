Frikanalen Beta
===============

Tools and web for the Norwegian public access TV channel [Frikanalen](http://www.frikanalen.no/).

Read `INSTALL.md` to get started with development.

[GitHub page](http://github.com/Frikanalen/) | [Project mailing list](http://lists.nuug.no/mailman/listinfo/frikanalen/)

License
-------
All under the GNU LGPL license, see the file [COPYING](COPYING) for more details.
