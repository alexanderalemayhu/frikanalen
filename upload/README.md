Upload app
==========

This uploads files. It's a Flask app.

Run like:

    PYTHONPATH=. FLASK_APP=fkupload FLASK_DEBUG=1 flask run

Setup
-----

It uses Python3. Do this to install first time:

    python3 -m venv env
    . env/bin/activate
    pip install -r requirements.txt
